package lexer

import "gitlab.com/Alomio/monkey/token"

//Lexer A monkey lexer
type Lexer struct {
	//TODO: Really it should be an io.READER so we can read in a file byte by byte
	input string
	//points to current chart
	position int
	// should be postion + 1
	readPosition int
	//current character under examination
	ch byte
}

//New Lexer instance
func New(input string) *Lexer {
	tokenizer := &Lexer{input: input}
	tokenizer.readChar()
	return tokenizer
}

func (lxr *Lexer) readChar() {
	if lxr.readPosition >= len(lxr.input) {
		lxr.ch = 0
	} else {
		lxr.ch = lxr.input[lxr.readPosition]
	}
	lxr.position = lxr.readPosition
	lxr.readPosition++
}

//NextToken Assign the next token
func (lxr *Lexer) NextToken() token.Token {
	var tok token.Token
	lxr.skipWhitespace()

	switch lxr.ch {
	case '=':
		tok = newToken(token.ASSIGN, lxr.ch)
	case ';':
		tok = newToken(token.SEMICOLON, lxr.ch)
	case '(':
		tok = newToken(token.LPAREN, lxr.ch)
	case ')':
		tok = newToken(token.RPAREN, lxr.ch)
	case ',':
		tok = newToken(token.COMMA, lxr.ch)
	case '+':
		tok = newToken(token.PLUS, lxr.ch)
	case '?':
		tok = newToken(token.QUESTION, lxr.ch)
	case '{':
		tok = newToken(token.LBRACE, lxr.ch)
	case '}':
		tok = newToken(token.RBRACE, lxr.ch)
	case 0:
		tok.Literal = ""
		tok.Type = token.EOF
	default:
		if isLetter(lxr.ch) {
			tok.Literal = lxr.readIdentifier()
			tok.Type = token.LookupIdent(tok.Literal)
			return tok
		} else if isDigit(lxr.ch) {
			tok.Type = token.INT
			tok.Literal = lxr.readNumber()
			return tok
		}
		tok = newToken(token.ILLEGAL, lxr.ch)
	}

	lxr.readChar()
	return tok
}

func newToken(tokenType token.TokenType, ch byte) token.Token {
	return token.Token{Type: tokenType, Literal: string(ch)}
}

//readIdentifier Keep track of the letters until we know if it's a keyword or identifier
func (lxr *Lexer) readIdentifier() string {
	position := lxr.position
	for isLetter(lxr.ch) {
		lxr.readChar()
	}
	return lxr.input[position:lxr.position]
}

//readRead Numbers until you hit a non-number character
func (lxr *Lexer) readNumber() string {
	position := lxr.position
	for isDigit(lxr.ch) {
		lxr.readChar()
	}
	return lxr.input[position:lxr.position]
}

//isDigit reads in ASCII Bytes and checks if they are a number
func isDigit(ch byte) bool {
	return '0' <= ch && ch <= '9'
}

func isLetter(ch byte) bool {
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
}

func (lxr *Lexer) skipWhitespace() {
	for lxr.ch == ' ' || lxr.ch == '\t' || lxr.ch == '\n' || lxr.ch == '\r' {
		lxr.readChar()
	}
}
