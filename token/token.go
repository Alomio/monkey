package token

//TODO: Possible improvement, use byte or int
type TokenType string

type Token struct {
	Type    TokenType
	Literal string
	//TODO: Add filename, line number and maybe column to hep erros codes later
}

const (
	ILLEGAL = "ILLEGAL"
	EOF     = "EOC"

	//Identifiers + literals
	IDENT = "IDENT"
	INT   = "INT"

	//Operators
	ASSIGN   = "="
	PLUS     = "+"
	QUESTION = "?"

	// Delimiters
	COMMA     = ","
	SEMICOLON = ";"

	LPAREN = "("
	RPAREN = ")"
	LBRACE = "{"
	RBRACE = "}"

	//Keywords
	FUNCTION = "FUNCTION"
	LET      = "LET"
)

//List of our keywords, may also be seen above
var keywords = map[string]TokenType{
	"fn":  FUNCTION,
	"let": LET,
}

//LookupIdent look up string to see if it's a keyword, if not it's an IDENT
func LookupIdent(ident string) TokenType {
	if tok, ok := keywords[ident]; ok {
		return tok
	}
	return IDENT
}
